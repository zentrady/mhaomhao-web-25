import axios from 'axios'
import humps from 'humps'
import { BASE_URL } from '../configs/constants'

const axiosInstance = axios.create({
  baseURL: `${BASE_URL}`,
  timeout: 120000
})

axiosInstance.interceptors.request.use((config) => {
  // Do something before request is sent
  if (config.headers['Content-Type'] !== 'multipart/form-data') {
    config.data = humps.decamelizeKeys(config.data)
    config.params = humps.decamelizeKeys(config.params)
  }
  return config
}, function (error) {
  // Do something with request error
  return Promise.reject(error)
})

// Add a response interceptor
axiosInstance.interceptors.response.use((response) => {
  // Do something with response data
  return humps.camelizeKeys(response.data)
}, function (error) {
  // const { response: { status } } = error
  // if (status === 401 || status === 403) {
  // }
  return Promise.reject(error)
})

class HttpRequest {
  constructor () {
    this.axios = axios
  }

  setHeader (header) {
    axiosInstance.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded'
    axiosInstance.defaults.headers.common[header.key] = header.value
  }
  setHeaderAuth (auth) {
    this.setHeader({
      key: 'Authorization',
      value: `token ${auth}`
    })
  }
  removeHeader () {
    delete axios.defaults.headers.common.Authorization
  }
  useLocalStorageTokenHeader () {
    if (process.client) {
      this.setHeader({
        key: 'Authorization',
        value: `token ${window.localStorage.getItem('token')}`
      })
    }
  }

  fetch (methodName, data) {
    return axiosInstance.get(methodName, {
      params: data
    })
  }

  create (methodName, data) {
    return axiosInstance.post(methodName, data)
  }

  update (methodName, data) {
    return axiosInstance.put(methodName, data)
  }

  edit (methodName, data) {
    return axiosInstance.patch(methodName, data)
  }

  remove (methodName, data) {
    return axiosInstance.delete(methodName, { data })
  }

  updateMultipart (methodName, formData) {
    return axiosInstance.put(methodName, formData,
      {
        headers: {
          Accept: 'application/json',
          'Content-Type': 'multipart/form-data'
        }
      })
  }
  createMultipart (methodName, data) {
    return axiosInstance.post(methodName, data, {
      headers: {
        Accept: 'application/json',
        'Content-Type': 'multipart/form-data'
      }
    })
  }
  request (type, url, data) {
    let promise = null
    switch (type) {
      case 'GET': promise = axios.get(url, { params: data }); break
      case 'POST': promise = axios.post(url, data); break
      case 'PUT': promise = axios.put(url, data); break
      case 'PATCH': promise = axios.patch(url, data); break
      case 'DELETE': promise = axios.delete(url, data); break
      default : promise = axios.get(url, { params: data }); break
    }
    return promise
  }
}

export default HttpRequest
